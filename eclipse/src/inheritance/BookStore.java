//Cathy Tham, 1944919

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] book=new Book[5];
		
		//fill the book array
		book[0]=new Book("The Hobbit", "J.R.R. Tolkien");
		book[1]=new ElectronicBook("The Little Prince","Antoine de Saint-Exup�ry",8);
		book[2]=new Book("Harry Potter and the Philosopher's Stone", "J.K Rowling");
		book[3]=new ElectronicBook("And Then There Were None", "Agatha Christie",6);
		book[4]=new ElectronicBook("The Adventures of Pinocchio", "Carlo Collodi",9);
		
		printArr(book);
		

	}
	
	public static void printArr(Book[] book){
        for(int i=0; i<book.length; i++){
            System.out.println(book[i].toString()); 
          }
        
	}
}
