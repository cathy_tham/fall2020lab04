//Cathy Tham, 1944919

package inheritance;

public class ElectronicBook extends Book {
	private int numberBytes;
	
	public ElectronicBook(String title,String author, int numberBytes) {
		super(title,author); 

		this.numberBytes=numberBytes;
		}
	
	public String toString(){
		String fromBase= super.toString();
		return (fromBase + " numberBytes: " + numberBytes);

	}
	
}
