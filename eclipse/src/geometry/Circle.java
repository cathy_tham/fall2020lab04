//Cathy Tham, 1944919

package geometry;
import java.lang.Math;

public class Circle implements Shape {
	private double radius;
	
	public Circle(double radius) {
		this.radius=radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		double pi=Math.PI;
		double area=Math.pow(radius,2)*pi;
		return area;
	}
	
	public double getPerimeter() {
		double pi=Math.PI;
		double diameter=radius*2;
		double perimeter=pi*diameter;
		return perimeter;
	}
}
