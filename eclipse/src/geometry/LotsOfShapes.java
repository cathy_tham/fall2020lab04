//Cathy Tham, 1944919

package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes=new Shape[5];
		
		//shapes[0]=new Shape();
		shapes[0]=new Rectangle(4.0,5.0);
		shapes[1]=new Rectangle(7.5,9.0);
		shapes[2]=new Circle(4.0);
		shapes[3]=new Circle(6.0);
		shapes[4]=new Square(5.0);
		
		printArr(shapes);
	}
	
	public static void printArr(Shape[] shapes) {
		for(int i=0 ;i<shapes.length;i++) {
			System.out.println("area: "+ shapes[i].getArea() + " perimeter: "+shapes[i].getPerimeter());
		
	}

	}
}
