//Cathy Tham, 1944919

package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
