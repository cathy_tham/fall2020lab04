//Cathy Tham, 1944919

package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	public Rectangle(double length, double width) {
		this.length=length;
		this.width=width;
	}
	
	public double getLength() {
		return length;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getArea() {
		double area=length*width;
		return area;
	}
	
	public double getPerimeter() {
		double perimeter=(length+width)*2;
		return perimeter;
	}

}
